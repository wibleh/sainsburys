<?php

require "vendor/autoload.php";

if (!isset($argv[1])  ||  empty($argv[1])) {
    echo "No URL parsed", PHP_EOL;
    exit(1);
}

function dd($str) { var_dump($str); exit; } // borrowed from Laravel, love this! Used whilst developing for testing

// sss -> validate input url?

try {
    $scraper = new Wibleh\Scraper\Scraper($argv[1]);
    $scraper->run();
    echo $scraper; // uses the magic method because why not
}
catch (Exception $e) { // could check for different exception types with multiple catch statements
    echo $e->getMessage(), PHP_EOL;
    exit(1); // not exit(0) because it wasn't a successful finish
}