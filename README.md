# Sainsbury's Technical Test for Andrew Holt

## Requirements

  * php: >= 5.3.2
  * curl >= ?.?.?

## Installation

  1. Install Composer
  1. Install packages via Composer i.e. `composer install`

## Running

A batch file, `run.bat` is included for testing on a windows environment.  This assumes that PHP is in the path.

Alternatively run the script by passing a single argument of the URL to process e.g. `php scrape.php [url]`.

### Saving Output

Output is sent straight to the terminal in this example.  To save this into a file, redirect the terminal output to a file e.g.

```
php scrape.php [url] > output.json
```

## PHPUnit Tests

PHPUNit tests can be run by using the command `phpunit` within the root of the project directory.  If the `phpunit` command is not available please run `composer global require phpunit/phpunit` to add it to your global composer installation, which should be set up within the PATH environment on your computer.

## Difficulties

  * No content being returned when requesting URLs. I switched to cURL to allow more control over what was sent.
  * Going OTT on the exercise and taking longer than I should have.

## To Do

  * Re-order the functions in the `Wibleh\Scraper\Scraper` class to make more sense
  * More exceptions
  * Better handling of errors
  * Use of Symfony console package for better outputting of debug etc
  * Returning the unit price and total as numbers rather than strings
  * Better handling of descriptions (Sainsbury's Ripe & Ready Red Pear x4 has more HTML in there than the others)
  * Addition of PHPDocs
  * Bulking out the PHPUnit tests

## Other Notes

  * Used PhpStorm 9.0.2 as my IDE
  * Developed on Windows
  * Tested on Windows and CentOS 6.7, both under PHP 5.5

