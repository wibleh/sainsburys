<?php namespace Wibleh\Scraper;

use Sunra\PhpSimple\HtmlDomParser;
use Wibleh\Scraper\Exceptions\HtmlFetchFailedException;
use Wibleh\Scraper\Exceptions\InvalidUrlException;
use Wibleh\Scraper\Exceptions\MissingUrlException;

/**
 * NOTES / TODO
 *
 * Fetch the initial page
 *
 * Parse for the following:
 *  div.product
 *    h3
 *      a -> url to fetch for the product's description; text for product title (minus <img> element)
 *    p.pricePerUnit -> ignore sub-elements
 *
 * Product description is within div.productText in the linked page
 *
 * Return JSON
 */

class Scraper
{
    protected $url;
    protected $data = array();

    public function __construct($url=null)
    {
        if (!is_null($url)) $this->setUrl($url);
    }

    public function __toString()
    {
        return json_encode($this->data);
    }

    public function getData()
    {
        return $this->data;
    }

    public function setUrl($url)
    {
        $this->validateUrl($url);

        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    protected function validateUrl($url)
    {
        do {
            if (!preg_match("/^https?\:\/\//", $url)) break;

            // @todo more validation rules here...

            return true;
        }
        while (false);

        throw new InvalidUrlException($url);
    }

    /**
     * Runs the scraper.
     *
     * @return bool
     * @throws HtmlFetchFailedException
     * @throws MissingUrlException
     * @throws \Exception
     */
    public function run()
    {
        if (empty($this->url)) throw new MissingUrlException();

        $data = array(); // $data = []; <-- nice shorthand, restricts version of PHP though
        $data['results'] = array();
        $data['total'] = 0;

        $response = $this->fetchUrl($this->url);

        foreach ($response->dom->find('div.product') as $product) {
            $result = $this->parseProduct($product);
            if ($result) $data['results'][] = $result;
        }

        foreach ($data['results'] as $result) $data['total'] += $result->unit_price;
        $data['total'] = $this->formatNumber($data['total']);

        $this->data = $data;

        return true;
    }

    protected function formatNumber($num)
    {
        return number_format($num, 2); // @todo return this as a decimal/float
    }

    public function parseProduct($dom)
    {
        $data = array();

        // title
        $title = $dom->find('h3 a');
        if (!count($title)) throw new \Exception('Title not found'); // @todo custom exception
        $a = $title[0]; // save this for getting the url for the description page
        $title = trim($title[0]->plaintext);

        // price
        $price = $dom->find('.pricePerUnit');
        if (!count($price)) throw new \Exception('Price not found'); // @todo custom exception
        $price = $price[0]->plaintext;
        if (!preg_match("/(\d+\.\d+)/", $price, $matches)) throw new \Exception('Invalid price format'); // @todo custom exception
        $price = $matches[1];
        $price = $this->formatNumber($price);

        // description
        $url = $a->href;
        $response = $this->fetchUrl($url);
        $description = $response->dom->find('.productText');
        if (!count($description)) throw new \Exception('Description not found');
        $description = $description[0];
        $description = $description->innertext; // @todo remove html via strip_tags() ?
        $description = trim($description);

        // size
        $size = $response->info['size_download'];
        $size = call_user_func(function() use ($size) {
            $sizes = array(
                'MB' => $size / pow(1024, 2),
                'KB' => $size / 1024,
                'B' => $size,
            );
            foreach ($sizes as $ext => $s) {
                if ($s >= 1) break; // if it reaches the end, $ext and $s will be the bytes value
            }
            return round($s, 2).$ext;
        });

        $data['title'] = $title;
        $data['description'] = $description;
        $data['unit_price'] = $price;
        $data['size'] = $size;

        return (object) $data;
    }

    protected function fetchUrl($url)
    {
        // didn't work, trying curl //$html = file_get_contents($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0");
        //curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        $cookieFile = tempnam(sys_get_temp_dir(), 'SBY');
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);

        $html = curl_exec($ch);

        $info = curl_getinfo($ch);

        $error = curl_error($ch);
        curl_close($ch);

        if ($error !== '') throw new HtmlFetchFailedException($url);

        $dom = HtmlDomParser::str_get_html($html);

        $return = new UrlResponse;
        $return->html = $html;
        $return->dom = $dom;
        $return->info = $info;

        return $return;
    }
}