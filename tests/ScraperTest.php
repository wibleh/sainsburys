<?php

use Wibleh\Scraper\Scraper;

class ScraperTest extends \PHPUnit_Framework_TestCase
{
    public function testScraperToString()
    {
        $scraper = new Scraper;

        $string = (string)$scraper;

        $this->assertTrue(is_string($string));
    }

    public function testDataIsArray()
    {
        $scraper = new Scraper;

        $data = $scraper->getData();

        $this->assertTrue(is_array($data));
    }

    public function testUrlIsStringViaConstructor()
    {
        $url = 'http://localhost/test/url';

        $scraper = new Scraper($url);

        $this->assertEquals($scraper->getUrl(), $url);
    }

    public function testUrlIsStringViaSetter()
    {
        $url = 'http://localhoset/test/url';

        $scraper = new Scraper;

        $scraper->setUrl($url);

        $this->assertEquals($scraper->getUrl(), $url);
    }

    public function testSecureUrl()
    {
        $url = 'https://localhost/test/url';

        $scraper = new Scraper($url);

        $this->assertEquals($scraper->getUrl(), $url);
    }

    public function testMailtoUrl()
    {
        $url = 'mailto:test@test.com';

        try {
            $scraper = new Scraper($url);

            $this->fail('No exception was thrown');
        } catch (\Wibleh\Scraper\Exceptions\InvalidUrlException $e) {
            // this is what's expected // assertTrue(true) ?
        } catch (Exception $e) {
            $this->fail('Invalid exception type thrown');
        }
    }

    public function testMalformedUrl()
    {

    }

    public function testUrlWithInternationalCharacters()
    {

    }

    public function testUrlWithRedirects()
    {

    }

    public function testProduct()
    {

    }

    public function testProductWithNoTitle()
    {

    }

    public function testProductWithHugeTitle()
    {

    }

    public function testProductWithNoDescription()
    {

    }

    public function testProductWithHugeDescription()
    {

    }

    public function testProductWithNoPrice()
    {

    }

    public function testProductWithAlphaNumericPrice()
    {

    }

    public function testProductWithPriceAsFloat()
    {

    }

    public function testProductWithNegativePrice()
    {

    }

    public function testProductWithZeroPrice()
    {

    }
}